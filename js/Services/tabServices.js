var tabServices = angular.module('tabServices', ['ngResource']);

//Summary:
//			When Tab.query() is called, it will get all the tabs from data/Tab/tabs.json
//			When Tab.get() is called, it will get all the details of tab with id=tabId from data/Tab/tabId.json
tabServices.factory('Tab', ['$resource',
  function($resource){
    return $resource('data/Tab/:tabId.json', {}, {
      query: {method:'GET', params:{tabId:'tabs'}, isArray:true}
    });
 }]);

/*
//Summary:
//			This service will store the information that has to be passed from form wizard to tab wizard
formServices.factory('storeTabInfo', [
	function() {
		//info : object
		//		info.selectedItem : object
		//						  if 'New' button is clicked, then this will be empty
		//						  if 'Edit' button is clicked, then this will contain the tab that has to be edited
		//		info.buttonClicked : string
		//						   will contain the button that has been clicked (eg. 'new')
		//		info.returnedItem : object
		//						  will contain the object that has to be returned to form wizard
	    var tabInfo = {};

	    tabInfo.selectedTab = {};
	    tabInfo.buttonClicked = null;
	    tabInfo.returnedTab = {};

	    tabInfo.setSelectedTab = function(tab) {
	      this.selectedTab = tab;
	    };

	    tabInfo.setButtonClicked = function(button){
	    	this.buttonClicked = button;
	    };

	    tabInfo.getSelectedTab = function() {
	      return this.selectedTab;
	    };

	    tabInfo.getButtonClicked = function(){
	    	return this.buttonClicked;
	    };

	    return tabInfo;
}]);*/