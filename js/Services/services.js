'use strict';

/* Services */

var services = angular.module('services', [
	'formServices',
	'fieldServices',
  'tabServices'
]);

//   .value('version', '0.1');