var fieldServices = angular.module('fieldServices', ['ngResource']);

fieldServices.factory('FieldService', ['$resource', function($resource){
    return $resource('mocks/data.json/:id', {id: '@m_id'});

    //use if you want to get a single object in a json file that has the path 'mocks/fields/{id}'
    //return $resource('mocks/fields/:id', {id: '@m_id'});
  }]);

  fieldServices.factory('FieldService1', ['$resource', function($resource){
    return $resource('mocks/fields/:id', {id: '@m_id'});
  }]);

  fieldServices.factory('SelectedFieldService', ['FieldService1', function(FieldService1) {
    var selectedField = {};

    selectedField.selectedId = '';
    selectedField.fieldDef = {};

    selectedField.selectField = function() {
      this.fieldDef = FieldService1.get({id: this.selectedId});
    };

    selectedField.changeSelectedId = function(newId) {
      this.selectedId = newId;
    };

    selectedField.saveField = function() {
      FieldService1.save({id: this.selectedId}, this.fieldDef);
    }

    return selectedField;

  }]);