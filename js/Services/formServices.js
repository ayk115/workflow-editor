var formServices = angular.module('formServices', ['ngResource']);


//Summary:
//			When Form.query() is called, it will get all the forms from data/Form/forms.json
//			When Form.get() is called, it will get all the details of form with id=formId from data/Form/formId.json
formServices.factory('Form', ['$resource',
  function($resource){
    return $resource('data/Form/:formId.json', {}, {
      query: {method:'GET', params:{formId:'forms'}, isArray:true}
    });
 }]);


//Summary:
//			When Field.query() is called, it will get all the fields from data/Field/fields.json
//			When Field.get() is called, it will get all the details of field with id=fieldId from data/Field/fieldId.json
formServices.factory('Field', ['$resource',
  function($resource){
    return $resource('data/Field/:fieldId.json', {}, {
      query: {method:'GET', params:{fieldId:'fields'}, isArray:true}
    });
 }]);


//Summary:
//			This service will store the information that has to be passed from component manager to form wizard
formServices.factory('storeInfo', [
	function() {
		//info : object
		//		info.selectedItem : object
		//						  if 'New' button is clicked, then this will be empty
		//						  if 'Edit' button is clicked, then this will contain the item that has to be edited
		//		info.chartType : string
		//					   will contain the chartType selected (eg. 'field')
		//		info.buttonClicked : string
		//						   will contain the button that has been clicked (eg. 'new')
		//		info.returnedItem : object
		//						  will contain the object that has to be returned to component manager
	    var info = {};

	    info.selectedItem = {};
	    info.chartType = null;
	    info.buttonClicked = null;
	    info.returnedItem = {};

	    info.setSelectedItem = function(item) {
	      this.selectedItem = item;
	    };

	    info.setChartType = function(chartType) {
	      this.chartType = chartType;
	    };

	    info.setButtonClicked = function(button){
	    	info.buttonClicked = button;
	    };

	    /*info.setReturnedItem = function(item){
	    	info.returnedItem = item;
	    };*/

	    info.getSelectedItem = function() {
	      return this.selectedItem;
	    };

	    info.getChartType = function() {
	      return this.chartType;
	    };

	    info.getButtonClicked = function(){
	    	return info.buttonClicked;
	    };

	    /*info.getReturnedItem = function(){
	    	return info.returnedItem;
	    };*/

	    return info;
}]);