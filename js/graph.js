w=null;
h = null;
graph = null;
paper = null;
paper2 = null;
curr_selected = null;
rect_w=70,rect_h=30;
rects=new Object();
texts=[];
cnt = 1;
dsteps =new Object();
dsteps_child =new Object();
asteps =new Object();
dcnt = 0;
parallel_inner_option = new Object();
parallel_inner_step = new Object();
parallel_outer = new Object;
var delete_flag = null;
links = [];
vert = new Object();
state = 0;
click_id = null;
pos_x = null;
pos_y = null;	
var divWidth=$("#Div").width();
w = divWidth;
graph = new joint.dia.Graph;
h=600;

//Creating paper 
paper = new joint.dia.Paper({
	el: $('#myholder'),
	width: w,
	height:h,
	model: graph,
	gridsize:4
});

//creating image
paper2 = new joint.dia.Paper({
	el: $('#myimage'),
	width: w,
	height:600,
	model: graph,
	interactive : false
});

//creating start node
start = new joint.shapes.basic.Circle({
	position:{ x:w/4,y:h/10},
	attrs:{circle:{fill:'black'}, text : {text:'Start',fill:'white'}}
});

//creating end node
end = new joint.shapes.basic.Circle({
	position:{ x:(w*6)/15,y:(4*h)/5},
	attrs:{circle:{fill:'black'}, text : {text:'End',fill:'white'}}
});
graph.addCells([start,end]);

//onclick event for any cell
paper.on('cell:pointerclick', function(cellView, evt, x, y) {
	//alert(dsteps_child[cellView.model.id]);
	//if(state==0)
	//{
		curr_selected=cellView.model;
		ret=curr_selected;
		if (ret)
		{
			if(!vert[ret.id])
			{
				onclick_join(ret,evt,x,y);
			}
		}
		else//Here we can join with the parent
		{
			paper.$el.removeClass('connecting');
			state = 0;
		}
	//}
});

//dragging event for any cell
paper.on('cell:pointerup',function(cellView, evt, x, y){
});

//onclick event for blank 
paper.on('blank:pointerclick', function(evt, x, y) {
	   color()
	   paper.$el.removeClass('connecting');
	   state=0;
});
//For setting the color of elements according to state=0
function color()
{
	   all_el = graph.getElements();
	   for (var i = 0; i <all_el.length; i++) {
			if (parallel_outer[all_el[i].id]) 
			{
				setColor(1,all_el[i]);
			}
			else
			{
				setColor(0,all_el[i]);
			}
		}; 
}

//Add simple step to graph;
function addProcess(step_name) 
{
	//alert(state);
	if(state==1)
	{
		if(parallel_inner_option[click_id.id])
		{
			cur_lane = graph.getCell(click_id.get('parent'));
			out_box = graph.getCell(cur_lane.get('parent'));
			rebuild_parallel(cur_lane,out_box);
		}
	}
	else
	{
		var rect = new joint.shapes.basic.Rect({
			position: { x: (100*w)/1500, y: (30*h)/500 },
			size: { width: rect_w, height:rect_h },
			attrs: { rect: { fill: '#035096',rx: 15, ry: 15}, text: { text: step_name, fill: 'white' } }
		});
		rects[rect.id]=1;
		graph.addCells([rect]);
		return rect.id;
	}
}


//Add manual decision step to graph;
function addMDstep(stepname) 
{
	dcnt+=1;
	var rect = new joint.shapes.basic.Path({
		size: { width: 70, height:70 },
		attrs: {
			path: { d: 'M 30 0 L 60 30 30 60 0 30 z' ,fill : '#9FA4EB'},
			text: {
				fill:'white',
				text: stepname,
				'ref-y': .5 // basic.Path text is originally positioned under the element
			}
		}
	});        
	dsteps[rect.id]=1;
	graph.addCells([rect]);
	return rect.id;
}

function DeleteStep() 
{
	if(state==1  && click_id.id!=start.id && click_id.id!=end.id )//&& dsteps_child[click_id.id]!=1 && parallel_inner_option[click_id.id]!=1)
	{
		if(parallel_inner_step[click_id.id]!=1)
		{
			click_id.remove();
			dsteps[click_id.id]=0;
			state=0;
			parallel_outer[click_id.id]=0;
			rects[click_id.id]=0;
		}
		else
		{
			delete_flag = click_id.id;
			cur_lane = graph.getCell(click_id.get('parent'));
			out_box = graph.getCell(cur_lane.get('parent'));
			rebuild_parallel(cur_lane,out_box);
		}
	}
}

function addADstep(stepname) 
{
	dcnt+=1;
	var rect = new joint.shapes.basic.Path({
		size: { width: 70, height:70 },
		attrs: {
			path: { d: 'M 30 0 L 60 30 30 60 0 30 z' ,fill : '#4369E5'},
			text: {
				text: stepname,
				'ref-y': .5 // basic.Path text is originally positioned under the element
			}
		}
	});
	asteps[rect.id]=1;
	graph.addCells([rect]);
	return rect.id;
}

function rebuild_parallel(lane,outer_box)
{
	//alert(curr_selected.id);
		var x1 = outer_box.get('position').x;
		var y1 = outer_box.get('position').y;
		var first_child = outer_box.get('embeds');
		var incoming = graph.getConnectedLinks(outer_box, { outbound: true });
		var outgoing = graph.getConnectedLinks(outer_box, { inbound: true });
		var ln_cnt=0;
		var mx=0;
		var lane_ar = new Object();
		var lane_step = new Object();
		var k=0;
		//alert(delete_flag.id);
		for(var i=0;i<first_child.length;i++)
		{
			if(!vert[first_child[i]])
			{
				var cur=[];
				var stp=graph.getCell(first_child[i]).get('embeds');
				lane_ar[k]=parseInt(stp.length);
				ln_cnt+=1;

				for(var j=0;j<stp.length;j++)
				{
					//alert([stp[j],curr_selected.id]);
					if(parallel_inner_option[click_id.id]==1 && click_id.id==stp[j] && first_child[i]==lane.id)
					{
						//alert('her');
							cur.push('step'+cnt);
					}
					else if(parallel_inner_step[stp[j]] && stp[j]!=delete_flag)
					{
						var vr=graph.getCell(stp[j]);
						var att=vr.get('attrs');
						cur.push(att['text'].text);
					}
				}
				if(first_child[i]==lane.id)
				{
					if(delete_flag!=null)
					{
						lane_ar[k]-=2;
					}
					else
					{
						lane_ar[k]+=2;//stp.length+1;
					}
				}
				if(lane_ar[k]>mx)
				{
					 mx=lane_ar[k];
				}   
				lane_step[k]=cur;
				k++;
				//alert(cnt);
			}
		}
		delete_flag=null;
		cnt+=1;
		mx+=2;
		var h= ((parseInt(mx))*parseInt(rect_h));//*w)/1500;
		var w = (((parseInt(ln_cnt))*rect_w+parseInt(10*parseInt(ln_cnt))));//*h)/500;
		var rect = new joint.shapes.basic.Rect({
				position: { x: x1, y: y1 },
				size: { width: w, height: h },
				attrs: { rect: {fill: '#3A9CF2' }, text: {} }
		});
		graph.addCells([rect]); 
		parallel_outer[rect.id]=1;
		outer_box.remove();
		if(incoming.length==1)
		{
			var src=incoming[0].get('target');
			//alert(src.id);
			var ln = new joint.dia.Link({
				source: { id: rect.id  }, target: { id: src.id},
				attrs: { '.marker-source': { d: 'M 10 0 L 0 5 L 10 10 z' } }
			});
			links.push([ln.id,src.id,rect.id]);
			graph.addCell([ln]);
		}
		if(outgoing.length==1)
		{
			var trgt = outgoing[0].get('source');
			var ln = new joint.dia.Link({
				source: { id: trgt.id }, target: { id: rect.id },
				attrs: { '.marker-source': { d: 'M 10 0 L 0 5 L 10 10 z' } }
			});
			links.push([ln.id,rect.id,trgt]);
			graph.addCell([ln]);
		}
		var prev=x1;
		//alert(k);
		for(var i=0;i<k;i++)
		{
			var rect2;
			//alert(lane_ar[k]);
			var st=0;
			if(i>0)
			{
				rect2 = new joint.shapes.basic.Rect({
					position: { x: parseInt(prev)+4, y: y1 },
					size: { width: rect_w, height: h },
					attrs: { rect: { fill: '#3A9CF2' }, text: {} }
				});
				var ln = new joint.shapes.basic.Rect({
					position: { x: parseInt(prev), y: y1 },
					size: { width: 2, height: h },
					attrs: { rect : { fill: '#3A9CF2'}, text: {} }
				});
				rect.embed(ln);
				graph.addCells([ln]);
				vert[ln.id]=1;
				paper.findViewByModel(ln).options.interactive = false; 
			}
			else
			{
				rect2 = new joint.shapes.basic.Rect({
					position: { x: parseInt(prev)+4, y: y1 },
					size: { width: rect_w, height: h },
					attrs: { rect: { fill: '#3A9CF2' }, text: {} }
				});
			}
			rect.embed(rect2);
			graph.addCells([rect2]);
			rect2.toBack();
			//alert(lane_ar[i]);
			var st_y=y1;
			for(var j=1;j<=lane_ar[i];j++)
			{
				if(j%2==1)
				{
					var click_rect = new joint.shapes.basic.Rect({
						position: { x: parseInt(prev)+(rect_w)/2, y: st_y+(rect_h)/2 },
						size: { width: 10, height: 10 },
						attrs: { rect: { fill:  '#3A9CF2'  }, text: {} }
					});
					st_y+=(rect_h/2+10);
					graph.addCells([click_rect]);
					click_rect.toFront();
					rect2.embed(click_rect);
					paper.findViewByModel(click_rect).options.interactive = false; 
					parallel_inner_option[click_rect.id]=1;
				}
				else
				{
					var step_rect = new joint.shapes.basic.Rect({
						position: { x: parseInt(prev)+5, y: st_y+(rect_h)/2 },
						size: { width: rect_w , height: rect_h },
						attrs: { rect: { fill:  '#3A9CF2'  }, text: {text:lane_step[i][st]} }
					});
					st+=1;
					st_y+=(rect_h/2+rect_h);
					rect2.embed(step_rect);
					graph.addCells([step_rect]);
					step_rect.toFront();
					parallel_inner_step[step_rect.id]=1;
					paper.findViewByModel(step_rect).options.interactive = false; 
				}
			}
			prev=parseInt(prev)+10+parseInt(rect_w);
			paper.findViewByModel(rect2).options.interactive = false; 
		};
		color();
		state=0;
}


function removeProcess() 
{
	rects[$scope.rprocess].remove();
}

function removeText() 
{
	texts[$scope.rtext].remove();
}

function addText(textdata) 
{
	var txt = new joint.shapes.basic.Text({
		position: {x:(100*w)/1500,y:(30*h)/500},
		size: { width:rect_w , height:rect_h},
		attrs: {text: {text: textdata }}
	});
	texts.push(txt);
	graph.addCells([txt]);
}

//Checking if link already exists in id1 and id2;
function check_link(cell1,cell2){
	var fl=1;
	var incoming = graph.getConnectedLinks(cell1, { outbound: true });
	var outgoing = graph.getConnectedLinks(cell1, { inbound: true });        
   	for(i=0;i<incoming.length;i++)
	{
		var src=incoming[i].get('target');
		if(src.id==cell2.id)
		{
			fl=0;
			break;
		}
	}
	for(i=0;i<outgoing.length;i++)
	{
		var trgt=outgoing[i].get('source');
		if(trgt.id==cell2.id)
		{
			fl=0;
			break;
		}
	}
	return fl;
}

//Setting color of the current cell as selected;
function setColor(par,cell){
		if(par && !vert[cell.id])
		{
			cell.attr({
				rect:{fill:'white'},
				circle:{fill:'black'},
				path:{fill:'#9FA4EB'},
				text: { fill: 'white', 'font-size': 15 }
			});  
			if(parallel_outer[cell.id])
			{
				cell.attr({rect:{fill: '#3A9CF2'}});
			} 
		}
		else
		{
			cell.attr({
			 rect:{fill:'#035096'},
			 circle:{fill:'black'},
			 text: { fill: 'white', 'font-size': 15 }
			});          
			if(asteps[cell.id])
			{
				cell.attr({path:{fill:'#4369E5'}});
			}
			if(dsteps[cell.id])
			{
				cell.attr({path:{fill:'#9FA4EB'}});
			}
			if(vert[cell.id])
			{
				cell.attr({rect:{fill:'black'}});
			} 
		}  
}

//Join two cells 
function onclick_join(cellView,ect,x,y) {
	//If state is zero then first block 
	 if(state==0)
	{
		cellView.attr({
			rect:{fill:'#E67E22'},
			circle:{fill:'#E67E22'},
			path:{fill:'#E67E22'},
			text: { fill: 'white', 'font-size': 15 }
		});
		paper.$el.addClass('connecting');
		click_id=cellView;
		pos_x=x;
		pos_y=y;
		state=1;
	}
	//Now join the previously selected cell with current cell
	else
	{
		 if(parallel_inner_option[click_id.id] || parallel_inner_step[click_id.id])
		 {
			
		 }
		//If previously selected cell is not same as current one
		else if(cellView.id!=click_id.id )
		{
			 var dec = 0;                

			 //Checking incoming for the decision step;A decision step should have only one 
			 if(dsteps[cellView.id])
			 {
				var incoming = graph.getConnectedLinks(cellView, { outbound: true });        
				if(incoming.length==1)
				{
					dec=1;
				}
			 }
			 //If previously clicked cell is decision step then It can not be linked with any other step
			 //If current cell is child of decision step
			 //If current cell is decision step and previously clicked cell is of option type
			 if (dsteps[click_id.id] || dsteps_child[cellView.id] || (dsteps_child[click_id.id] && dsteps[cellView.id]))
			 {
					dec = 1;
			 }
			 //If previously clicked cell is child of decision step
			 if(dsteps_child[click_id.id])
			 {
				var outgoing = graph.getConnectedLinks(click_id, { inbound: true });        
				if(outgoing.length==1)
				{
					dec=1;
				}                        
			 }
			 if(parallel_outer[cellView.id])
			 {
				var incoming = graph.getConnectedLinks(cellView, { outbound: true });        
				if(incoming.length==1)
				{
					dec=1;
				}
			 }
			 if (parallel_outer[click_id.id])
			 {
				var outgoing = graph.getConnectedLinks(click_id, { inbound: true });        
				if(outgoing.length==1)
				{
					dec = 1;
				}
			 }
			 //A normal step can't have more than one incoming;
			 if(rects[cellView.id])
			 {
				var incoming = graph.getConnectedLinks(cellView, { outbound: true });        
				if(incoming.length==1)
				{
					dec=1;
				}
			 }
			 //A normail step can't have more than one outgoing;
			 if (rects[click_id.id])
			 {
				var outgoing = graph.getConnectedLinks(click_id, { inbound: true });        
				if(outgoing.length==1)
				{
					dec = 1;
				}
			 }
			 if(parallel_inner_step[cellView.id] || parallel_inner_option[cellView.id])
			 {
			 	dec=1;
			 }
			 if(dec==1 || cellView.id==start.id)
			 {

			 }
			 else if(check_link(cellView,click_id))
			 {                    
				var ln = new joint.dia.Link({
					source: { id: cellView.id }, target: { id: click_id.id },
					attrs: { '.marker-source': { d: 'M 10 0 L 0 5 L 10 10 z' } }
				});
				links.push([ln.id,cellView.id,click_id.id]);
				graph.addCell([ln]);
			 }
			color();
			paper.$el.removeClass('connecting');
			state=0;
		}
	}           
}

function ondrag_join (cellView,evt,x,y) {
	var elementBelow = graph.get('cells').find(function(cell) {
		if (cell instanceof joint.dia.Link) return false; // Not interested in links.
		if (cell.id === cellView.model.id) return false; // The same element as the dropped one.
		if (cell.getBBox().containsPoint(g.point(x, y))) {
			return true;
		}
		return false;
	});

	if (elementBelow && !_.contains(graph.getNeighbors(elementBelow), cellView.model)) {
	graph.addCell(new joint.dia.Link({
		source: { id: cellView.model.id }, target: { id: elementBelow.id },
		attrs: { '.marker-source': { d: 'M 10 0 L 0 5 L 10 10 z' } }
	}));
		// Move the element a bit to the side.
		cellView.model.translate(-200, 0);
	}
}

function add_rect(x_val,y_val){
	var rect = new joint.shapes.basic.Rect({
			position: { x: x_val, y: y_val },
			size: { width: rect_w, height: rect_h },
			attrs: { rect: { fill: '#3A9CF2',rx: 15, ry: 15}, text: {} }
	});
	graph.addCells([rect]);
}

//$scope.draw_line =function  (){
function draw_line(cell,x,y){
	var mo=graph.findModelsFromPoint(g.point(x,y));
	var all_el = graph.getElements();
	for (var i = 0; i < all_el.length; i++) {
		if(parallel_outer[all_el[i].id])
		{
			var emb = (all_el[i].get('embeds'));
			var cnt=0;
			for (var j = 0; j <emb.length ;  j++) {
				if(!vert[emb[j]] && mo[0].id==emb[j])
				{
					var cl=graph.getCell(emb[j]);
					return cl;
				}
			};
		}
	};
	return 0;
}

var out_h,out_w;
function outer_box(x_val,y_val,steps)
{
	//alert(steps);
	//out_h= (((parseInt($scope.parallel_steps)+1))*parseInt(rect_h));
	out_h=rect_h;
	out_w = (((parseInt(steps))*rect_w+parseInt(10*parseInt(steps))));
	var rect = new joint.shapes.basic.Rect({
			position: { x: x_val, y: y_val },
			size: { width: out_w, height: out_h },
			attrs: { rect: { }, text: {} }
	});
	graph.addCells([rect]); 
	parallel_outer[rect.id]=1;
	var prev=x_val;
	for (var i = 0; i < (parseInt(steps)); i++) {
		var rect2;
		if(i>0)
		{
			rect2 = new joint.shapes.basic.Rect({
				position: { x: parseInt(prev)+10, y: y_val },
				size: { width: rect_w, height: out_h },
				attrs: { rect: { fill: '' }, text: {} }
			});
			var ln = new joint.shapes.basic.Rect({
				position: { x: parseInt(prev)+5, y: y_val },
				size: { width: 2, height: out_h },
				attrs: { rect : { fill: 'black'}, text: {} }
			}); 
			rect.embed(ln);
			graph.addCells([ln]);
			vert[ln.id]=1;
			paper.findViewByModel(ln).options.interactive = false; 
		}
		else
		{
			rect2 = new joint.shapes.basic.Rect({
				position: { x: parseInt(prev)+5, y: y_val },
				size: { width: rect_w, height: out_h },
				attrs: { rect: { fill: '' }, text: {} }
			});
		}
		rect3 = new joint.shapes.basic.Rect({
			position: { x: parseInt(prev)+(rect_w)/2, y: y_val +(rect_h)/2 },
			size: { width: 10, height: 10 },
			attrs: { rect: { fill: '#3A9CF2' }, text: {} }
		});
		prev=parseInt(prev)+10+parseInt(rect_w);
		rect.embed(rect2);
		rect2.embed(rect3);
		rect3.toFront();
		graph.addCells([rect2,rect3]);
		parallel_inner_option[rect3.id]=1;
		rect2.toBack();    
		paper.findViewByModel(rect2).options.interactive = false; 
		paper.findViewByModel(rect3).options.interactive = false; 
	};
	color();
	//cnt+=1;
}

function is_child(cellView)
{
	var all = graph.getElements();
	for (var i =0;i < all.length ; i++) {
		if(parallel_outer[all[i].id]==1)
		{
			var emb = (all[i].get('embeds'));
			for(var j=0 ;j<emb.length ;j++ )
			{
				if(cellView.model.id==emb[j])
				{
					return 0;
				}
			}
		}
	};
	return 1;
}

function add_parallel(lanes)
{
	var steps = lanes;
	var st_x = 100;st_y=100;
	if(steps==0)
	{
		alert('Atleast one step is required!!!');
	}
	else
	{
		outer_box((st_x*w)/1500,(st_y*h)/500,steps);
	}
}

function verify()
{
	//Start step should have an outgoing link
	var inboundStart = graph.getConnectedLinks(start, { inbound: true });
	var outboundEnd = graph.getConnectedLinks(end, { outbound: true });
	var all_el = graph.getElements();
	var flag=0;
	for (var i = 0; i <all_el.length; i++) {
			if(rects[all_el[i].id])
			{
				var incoming = graph.getConnectedLinks(all_el[i], { outbound: true });        
				var outgoing = graph.getConnectedLinks(all_el[i], { inbound: true });        
				if(!(incoming.length==1 && outgoing.length==1))
				{
					flag=1;
					alert("Error from normal step!!!");
					break;
				}
			}
			else if(dsteps_child[all_el[i].id])
			{
				var outgoing = graph.getConnectedLinks(all_el[i], { inbound: true });        		
				if(outgoing.length!=1)
				{
					flag=1;
					alert("No link from decision steps!!!");
					break;
				}
			}
			else if(parallel_outer[all_el[i].id])
			{
				var incoming = graph.getConnectedLinks(all_el[i], { outbound: true });        
				var outgoing = graph.getConnectedLinks(all_el[i], { inbound: true });        
				if(!(incoming.length==1 && outgoing.length==1))
				{
					flag=1;
					alert("Error from parallel step!!!");
					break;
				}
				var first_child = all_el[i].get('embeds');
				//alert(first_child.length);
				for(var i=0;i<first_child.length;i++)
				{
					if(!vert[first_child[i]])
					{
						stp=graph.getCell(first_child[i]).get('embeds');
						if(stp.length<2)
						{
							flag=1;
							alert("Error from lane step!!!");
							break;
						}
					}
				}
			}
			if(flag==1)
			{
				break;
			}
	};
	//alert(flag);
	if(flag==1)
	{

	}
	else if(inboundStart.length!=1)
	{
		alert("Error from start");
	}
	else if(outboundEnd.length==0)
	{
		alert("No link to end");
	}
	else
	{
		alert("No Error");
	}
}


function createNew()
{        
	if(dsteps[curr_selected.id])
	{
		var connected=curr_selected.get('embeds'); 
		var options_steps = new Object();
		var opt = 1;
		if(connected)
		{
			opt=connected.length+1;
			for (var i =0 ;i<connected.length;i++) {
				var cur_cell=graph.getCell(connected[i]);
				var con_step=graph.getConnectedLinks(cur_cell, { inbound: true });
				if(con_step.length>0)
				{
					options_steps[i]=con_step[0].get('source');
				}
				cur_cell.remove();
			};

		} 
		var middle=parseInt(opt/2);
		for(var i=0;i<middle;i++)
		{
			var rect = new joint.shapes.basic.Rect({
				position: { x: parseInt(curr_selected.get('position').x)-(i+1)*30 , y: parseInt(curr_selected.get('position').y)+40+rect_w  },
				size: { width: 15, height: 15},
				attrs: { rect: { fill: '#3A9CF2',rx: 15, ry: 15}, text: {} }
			}); 
			var ln = new joint.dia.Link({
				source: { id: rect.id }, target: { id: curr_selected.id },
				 attrs: { '.marker-source': { d: 'M 10 0 L 0 5 L 10 10 z' } }
			});	
			curr_selected.embed(rect);
			dsteps_child[rect.id]=1;
			graph.addCells([rect,ln]); 
			V(paper.findViewByModel(ln).el).addClass('special-link');
			//paper.findViewByModel(ln).addClass('special-link');	 	
			if(options_steps[i])
			{
				var ln2 = new joint.dia.Link({
					 source: { id: options_steps[i].id }, target: { id: rect.id },
					 attrs: { '.marker-source': { d: 'M 10 0 L 0 5 L 10 10 z' } }
				});			
				graph.addCells([ln2]);		
			}
		}
		for(var i=middle+1;i<=opt;i++)
		{
			var rect = new joint.shapes.basic.Rect({
				position: { x: parseInt(curr_selected.get('position').x)+(i+1-middle)*30 , y: parseInt(curr_selected.get('position').y)+40+rect_w  },
				size: { width: 15, height: 15},
				attrs: { rect: { fill: '#3A9CF2',rx: 15, ry: 15}, text: {} }
			}); 
			var ln = new joint.dia.Link({
				source: { id: rect.id }, target: { id: curr_selected.id },
				 attrs: { '.marker-source': { d: 'M 10 0 L 0 5 L 10 10 z' } }
			});
			curr_selected.embed(rect);
			dsteps_child[rect.id]=1;
			graph.addCells([rect,ln]);
			V(paper.findViewByModel(ln).el).addClass('special-link'); 
			if(options_steps[i-1])
			{
				var ln2 = new joint.dia.Link({
					 source: { id: options_steps[i-1].id }, target: { id: rect.id },
					 attrs: { '.marker-source': { d: 'M 10 0 L 0 5 L 10 10 z' } }
				});			
				graph.addCells([ln2]);		
			}
		}
		color();
	}
}
