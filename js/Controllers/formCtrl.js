var formCtrl = angular.module('formCtrl', []);

formCtrl.controller('formWizard', ['$scope', 'Tab', 'storeInfo', 'ngDialog',
  function($scope, Tab, info, ngDialog) {

    $scope.tabs = [{
            title: 'General',
            url: 'generalTab'
        }, {
            title: 'Tabs',
            url: 'tabsTab'
    }];

    $scope.currentTab = 'generalTab';
    info.returnedItem = info.getSelectedItem();
    $scope.filterList = "search";
    $scope.display = Tab.query();
    $scope.tabsAdded = [];
    $scope.selectedTab = null;


    //Summary:
    //          Called when form wizard pop-up opens
    //          Sets all the data for the form wizard
    $scope.$on('ngDialog.opened', function (e, $dialog) {

      if(info.buttonClicked == 'edit'){
        var item = info.returnedItem;

        if(item.m_name){
          $scope.data.m_name = item.m_name;
        }
        else{
          $scope.data.m_name = "";
        }

        if(item.m_description){
          $scope.data.m_description = item.m_description;
        }
        else{
          $scope.data.m_description = "";
        }

        if(item.m_id){
          $scope.data.m_id = item.m_id;
        }
        else{
          $scope.data.m_id = null;
        }

        if(item.m_tabs){
          $scope.tabsAdded = item.m_tabs;
        }
      }
      else if(info.buttonClicked == 'new'){
        $scope.data = {m_id:null, m_name:"", m_description:""};
        //assign id
      }

    });


    //Summary:
    //          Called when the form wizard pop-up is closed
    //          Adds the new form to component manager
    $scope.$on('ngDialog.closed', function (e, $dialog) {
      var parentScope = $scope.$parent;
      parentScope.$apply(function(){
        if(!angular.equals(info.returnedItem,{})){
          var item = info.returnedItem;
          parentScope.itemsAdded.push({"m_id":item.m_id, "m_name":item.m_name});
          //store tabsAdded and other info in form description file
        }
      });
      /*var copy = angular.copy(info.returnedItem);
      parentScope.itemsAdded.push(copy);
      parentScope.removeItem(copy);*/
    });

    $scope.onClickTab = function (tab) {
        $scope.currentTab = tab.url;
    };

    $scope.tabsClickable = function(tab){
      if(tab.title == "Tabs"){
        return angular.equals(info.returnedItem, {});
      }
      else{
        return false;
      }
    }
    
    $scope.isActiveTab = function(tabUrl) {
        return tabUrl == $scope.currentTab;
    };


    //Summary:
    //          Called when 'Save' button is clicked in General tab
    $scope.update = function() {
      info.returnedItem = angular.copy($scope.data);
      $scope.reset();
    };


    //Summary:
    //          Called when 'Reset' button is clicked in General Tab
    $scope.reset = function() {
      $scope.data = {m_id:null, m_name:"", m_description:""};
    };


    //Summary:
    //          Checks whether some data has been filled in General tab
    $scope.isUnchanged = function() {
      return (($scope.data.m_name==undefined || $scope.data.m_name=="") && $scope.data.m_description=="");
    };

    // Summary:
    //          Returns true if "Search" option is selected in left panel
    //          Returns fasle if "All" option is selected in left panel
    $scope.isSearchSelected = function(){
      if($scope.filterList == 'search'){
        return true;
      }
      else{
        $scope.filterDisplay = "";
        return false;
      }
    };

    // Summary:
    //          Adds an item to $scope.tabsAdded only if that tab is not present in $scope.tabsAdded
    $scope.addTab = function(tab){
      if ($scope.tabsAdded.indexOf(tab) == -1){
        var tabCopy = angular.copy(tab);
        //change id of tab
        $scope.tabsAdded.push(tabCopy);
      }
    };


    // Summary:
    //          Removes a tab from $scope.tabsAdded
    $scope.removeTab = function(tab){
      var index = $scope.tabsAdded.indexOf(tab);
      if (index > -1){
        if($scope.isSelectedTab(tab)){
          $scope.selectedTab = null;
        }
        if($scope.display.indexOf(tab)==-1){
          $scope.display.push(tab);
        }
        $scope.tabsAdded.splice(index,1);
      }
    };

    // Summary:
    //          Called when an tab is clicked in right panel
    $scope.selectTab = function(tab){
      $scope.selectedTab = tab;
    };


    // Summary:
    //          Returns true or false based on whether current tab is selected or not
    $scope.isSelectedTab = function(tab) {
      return $scope.selectedTab === tab;
    };

    // Summary:
    //          Checks whether any tab has been selected or not and returns true or false based on that
    $scope.anyTabSelected = function(){
      if($scope.selectedTab == null){
        return true;
      }
      else{
        return false;
      }
    };

    /*


    // Summary:
    //          Called when New button is clicked in form wizard
    //          Sets the tabInfo(like tab selected) that has to be send to tab wizard.
    $scope.newTab = function(){

      tabInfo.setButtonClicked('new');

      var newTab = {};
      tabInfo.setSelectedTab(newTab);
      $scope.tabPopUp();

    };


    // Summary:
    //          Called when Edit button is clicked in form wizard
    //          Sets the tabInfo(like tab selected) that has to be send to tab wizard.
    $scope.editTab = function(){

      if($scope.selectedTab==null){
        return;
      }

      tabInfo.setButtonClicked('edit');
      var tab = Tab.get({tabId: $scope.selectedTab.m_id}, function(data){
        tabInfo.setSelectedTab(data);

        var index = $scope.tabsAdded.indexOf($scope.selectedTab);
        if (index > -1){
          if($scope.isSelectedTab($scope.selectedTab)){
            $scope.selectedTab = null;
          }
          $scope.tabsAdded.splice(index,1);
        }

        $scope.tabPopUp();
      });
    };*/

    $scope.reset();

}]);