'use strict';

/* Controllers */

var controllers = angular.module('controllers', [
  'ngDialog',
  'formCtrl',
  'fieldCtrl',
  'tabCtrl'
]);

controllers.controller('chartTypeCtrl', ['$scope', 'Field', 'Form', 'Tab', 'storeInfo', 'ngDialog',
  function($scope, Field, Form, Tab, info, ngDialog) {

    $scope.display = {};
    $scope.selectedItem = null;
    $scope.chartType = null;
    $scope.filterList = "all";
    $scope.itemsAdded = [];
    $scope.fieldsAdded = [];
    $scope.formsAdded = [];
    $scope.tabsAdded = [];


    // Summary:
    //          To check if field is selected or not
    $scope.isField = function(){
      if($scope.chartType == 'field'){
        return true;
      }
      else{
        return false;
      }
    };


    // Summary: 
    //          To check if form is selected or not
    $scope.isForm = function(){
      if($scope.chartType == 'form'){
        return true;
      }
      else{
        return false;
      }
    };


    //Summary:
    //          To check if chartType selected is 'tab' or not
    $scope.isTab = function(){
      return $scope.chartType == 'tab';
    };


    // Summary:
    //            Called when chartType has been changed.
    //            Will set the $scope.display depending on which chartType has been selected.
    $scope.$watch("chartType", function() {

      if($scope.isField()){
        $scope.display = Field.query();
        $scope.itemsAdded = $scope.fieldsAdded;
      }
      else if($scope.isForm()){
        $scope.display = Form.query();
        $scope.itemsAdded = $scope.formsAdded;
      }
      else if($scope.isTab()){
        $scope.display = Tab.query();
        $scope.itemsAdded = $scope.tabsAdded;
      }
      else{
        $scope.display = {};
        $scope.itemsAdded = [];
      }

    }, true);


    // Summary:
    //          Returns true if "Search" option is selected in left panel
    //          Returns fasle if "All" option is selected in left panel
    $scope.isSearchSelected = function(){
      if($scope.filterList == 'search'){
        return true;
      }
      else{
        $scope.filterDisplay = "";
        return false;
      }
    };


    // Summary:
    //          Called when an item is clicked in right panel
    $scope.selectItem = function(item){
      $scope.selectedItem = item;
    };


    // Summary:
    //          Returns true or false based on whether current item is selected or not
    $scope.isSelectedItem = function(item) {
      return $scope.selectedItem === item;
    };


    // Summary:
    //          Checks whether any item has been selected or not and returns true or false based on that
    $scope.anyItemSelected = function(){
      if($scope.selectedItem == null){
        return true;
      }
      else{
        return false;
      }
    };


    // Summary:
    //          Adds an item to $scope.itemsAdded only if that item is not present in $scope.itemsAdded
    $scope.addItem = function(item){
      if ($scope.itemsAdded.indexOf(item) == -1){
        var index = $scope.display.indexOf(item);
        $scope.display.splice(index,1);
        $scope.itemsAdded.push(item);
      }
    };


    // Summary:
    //          Removes an item from $scope.itemsAdded
    $scope.removeItem = function(item){
      var index = $scope.itemsAdded.indexOf(item);
      if (index > -1){
        if($scope.isSelectedItem(item)){
          $scope.selectedItem = null;
        }
        $scope.display.push(item);
        $scope.itemsAdded.splice(index,1);
      }
    };


    // Summary:
    //          creates a copy of an item in component manager
    $scope.duplicate = function(){

      if($scope.selectedItem == null){
        return;
      }

      var item = angular.copy($scope.selectedItem);
      /*if(item.m_name){
        item.m_name += '1';
      }*/

      $scope.itemsAdded.push(item);
    };


    //Summary:
    //          Opens a pop-up when chartType=='field'
    $scope.fieldPopUp = function () {
      ngDialog.open({
        template: '../../templates/fieldWizard.html',
        controller: '',
        className: 'ngdialog-theme-default'
      });
    };


    //Summary:
    //          Opens a pop-up when chartType=='form'
    $scope.formPopUp = function () {
      ngDialog.open({
        template: '../../templates/formWizard.html',
        controller: 'formWizard',
        className: 'ngdialog-theme-default',
        scope: $scope
      });
    };


    //Summary:
    //          Opens a pop-up when chartType=='tab'
    $scope.tabPopUp = function () {
      ngDialog.open({
        template: '../../templates/TabWizard.html',
        controller: 'tabWizard',
        className: 'ngdialog-theme-default',
        scope: $scope
      });
    };


    // Summary:
    //          Called when New button is clicked in component manager
    //          Sets the info that has to be send like chartType.
    $scope.new = function(){

      if($scope.chartType==null){
        return;
      }

      info.setChartType($scope.chartType);
      info.setButtonClicked('new');

      if($scope.isField()){
        $scope.fieldPopUp();
      }
      else if($scope.isForm()){
        var newForm = {};
        info.setSelectedItem(newForm);
        $scope.formPopUp();
      }
      else if($scope.isTab()){
        var newTab = {};
        info.setSelectedItem(newTab);
        $scope.tabPopUp();
      }

    };


    //Summary:
    //          Delete the selected Item
    $scope.deleteSelectedItem = function(){
      var index = $scope.itemsAdded.indexOf($scope.selectedItem);
      if (index > -1){
        if($scope.isSelectedItem($scope.selectedItem)){
          $scope.selectedItem = null;
        }
        $scope.itemsAdded.splice(index,1);
      }
    }


    // Summary:
    //          Called when Edit button is clicked in component manager
    //          Sets the info that has to be send like chartType, selectedItem.
    $scope.edit = function(){

      if($scope.chartType==null || $scope.selectedItem==null){
        return;
      }

      info.setChartType($scope.chartType);
      info.setButtonClicked('edit');

      if($scope.isForm()){
        var item = Form.get({formId: $scope.selectedItem.m_id}, function(data){
          info.setSelectedItem(data);
          $scope.deleteSelectedItem();
          $scope.formPopUp();
        });
      }

      else if($scope.isField()){
        $scope.deleteSelectedItem();
        $scope.fieldPopUp();
      }

      else if($scope.isTab()){
        var item = Tab.get({tabId: $scope.selectedItem.m_id}, function(data){
          info.setSelectedItem(data);
          $scope.deleteSelectedItem();
          $scope.tabPopUp();
        });
      }

    };


    // Summary:
    //          Checks whether any chartType has been selected or not and returns true or false based on that
    $scope.anyChartTypeSelected = function(){
      if($scope.chartType == null){
        return true;
      }
      else{
        return false;
      }
    };

}]);