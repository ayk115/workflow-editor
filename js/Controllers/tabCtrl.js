var tabCtrl = angular.module('tabCtrl', []);

tabCtrl.controller('tabWizard', ['$scope', 'storeInfo',
	function($scope, info){
		$scope.tabs = [{
            title: 'General',
            url: 'generalTab'
        }, {
            title: 'Widgets',
            url: 'widgetsTab'
    }];

    $scope.currentTab = 'generalTab';
    info.returnedItem = info.getSelectedItem();
    $scope.itemsAdded = [];


    //Summary:
    //          Called when tab wizard pop-up opens
    //          Sets all the data for the tab wizard
    $scope.$on('ngDialog.opened', function (e, $dialog) {

      if(info.buttonClicked == 'edit'){
        var tab = info.returnedItem;

        if(tab.m_name){
          $scope.data.m_name = tab.m_name;
        }
        else{
          $scope.data.m_name = "";
        }

        if(tab.m_description){
          $scope.data.m_description = tab.m_description;
        }
        else{
          $scope.data.m_description = "";
        }

        if(tab.m_id){
          $scope.data.m_id = tab.m_id;
        }
        else{
          $scope.data.m_id = null;
        }

        if(tab.m_items){
          $scope.itemsAdded = tab.m_items;
        }
      }
      else if(info.buttonClicked == 'new'){
        $scope.data = {m_id:null, m_name:"", m_description:""};
        //assign id
      }

    });


    //Summary:
    //          Called when the tab wizard pop-up is closed
    //          Adds the new tab to component manager
    $scope.$on('ngDialog.closed', function (e, $dialog) {
      var parentScope = $scope.$parent;
      parentScope.$apply(function(){
        if(!angular.equals(info.returnedItem,{})){
          var item = info.returnedItem;
          parentScope.itemsAdded.push({"m_id":item.m_id, "m_name":item.m_name});
          //store widgetsAdded and other info in form description file
        }
      });
    });

    $scope.onClickTab = function (tab) {
        $scope.currentTab = tab.url;
    };

    $scope.tabsClickable = function(tab){
      if(tab.title == "Tabs"){
        return angular.equals(info.returnedItem, {});
      }
      else{
        return false;
      }
    }
    
    $scope.isActiveTab = function(tabUrl) {
        return tabUrl == $scope.currentTab;
    };

    $scope.reset = function() {
      $scope.data = {m_id:null, m_name:"", m_description:""};
    };

    //Summary:
    //          Called when 'Save' button is clicked in General tab
    $scope.update = function() {
      info.returnedItem = angular.copy($scope.data);
      $scope.reset();
    };


    //Summary:
    //          Called when 'Reset' button is clicked in General Tab
    $scope.reset = function() {
      $scope.data = {m_id:null, m_name:"", m_description:""};
    };


    //Summary:
    //          Checks whether some data has been filled in General tab
    $scope.isUnchanged = function() {
      return (($scope.data.m_name==undefined || $scope.data.m_name=="") && $scope.data.m_description=="");
    };

    $scope.reset();
}]);