var fieldCtrl = angular.module('fieldCtrl', []);

fieldCtrl.controller('ListCtrl', function() {
    this.list = [{label: "label", value: 1}, {label: "label1", value: 2}];

    this.addValue = function() {
    this.list.push({label:"label", value: "1"});
    };

    this.removeValue = function(valueToRemove) {
      var index = this.list.indexOf(valueToRemove);
      this.list.splice(index, 1);
    };

    this.clearValue = function(value) {
      value.label = '';
      value.value = '';
    };
  });

  fieldCtrl.controller("FieldIndexCtrl", ['FieldService', function(FieldService) {
    this.fields = FieldService.query();

    //use it to get a single object
    //this.field1 = FieldService.get({id: 1});

  }]); 

   

  fieldCtrl.controller("MainPanelCtrl", ['SelectedFieldService', function(SelectedFieldService) {
    this.editField = function() {
      SelectedFieldService.selectField();
      this.fieldDef = SelectedFieldService.fieldDef;
    };

    this.newField = function() {
      SelectedFieldService.fieldDef = {};
      this.fieldDef = SelectedFieldService.fieldDef;
    };

    this.saveField = function() {
      SelectedFieldService.fieldDef = this.fieldDef;
      SelectedFieldService.saveField();
    };

    this.editScript = function() {
      document.getElementById('scriptPopUp').style.display='block';
      document.getElementById('fade').style.display='block';
    };

    this.closePopUp = function() {
      document.getElementById('scriptPopUp').style.display='none';
      document.getElementById('fade').style.display='none';
    };
  }]);

  fieldCtrl.controller("ScriptEditorCtrl", ['FieldService', function(FieldService) {
    this.mathFunc = ["+", "-", "*", "/", "(", ")"];
    this.fieldList = FieldService.query();

    this.selectField = function(field) {
      this.selectedField = field;
      SelectedFieldService.changeSelectedId(field.m_id);
    };

    this.isSelectedField = function(field) {
      return this.selectedField === field;
    };

    this.add = function(value) {
      document.getElementById('script').innerHTML += value;
    };
  }]);