var controllers = angular.module('controllers', []);

controllers.controller('editor', ['$scope','readFormData','$http' , function($scope , readFormData, $http) {
	///////////////
	$scope.formDisable = true;
	var graph_id = null;
	var graph_json = null;
	$scope.space = " ";
	var steps = [];
	$scope.step = null;
	var stepcount = 1;
	function newStep(id, graph_id, stepname, description, type, in_graph_id)
	{
		this.id = id;
		this.graph_id = graph_id;
		this.stepname = stepname;
		this.description = description;
		this.type = type;
		this.in_graph_id = in_graph_id;
		this.field_type = null;
	}
	
	$scope.tab_url = null;
	
	var tasks = {};
	$scope.curr_tasks = [];
	$scope.task = null;
	$scope.curr_task_desc = null;
	$scope.show_tasks = null;
	var taskcount = 1;
	function newTask(id, taskname, step_id, description)
	{
		this.id = id;
		this.taskname = taskname;
		this.step_id = step_id;
		this.description = description;
	}
	
	var options = {};
	$scope.curr_options = [];
	$scope.option = null;
	$scope.curr_option_desc = null;
	$scope.show_options = null;
	var optioncount = 1;
	function newOption(id, optionname, step_id, description)
	{
		this.id = id;
		this.optionname = optionname;
		this.step_id = id;
		this.description = description;
		this.validation = null;
	}
	
	$scope.fields = [
	{name: 'Integer', url: 'templates/integer.html'}, 
	{name: 'Text', url: 'templates/text.html'},
	{name: 'Decimal', url: 'templates/decimal.html'},
	{name: 'Date', url: 'templates/date.html'}, 
	{name: 'Enueration', url: 'templates/enumeration.html'}];
	$scope.field_selected = $scope.fields[0];
	
	$scope.task_type = [
	{name: 'Forms', url: 'templates/forms.html'},
	{name: 'Datacards', url: 'templates/datacards.html'},
	{name: 'Update Field', url: 'templates/update_field.html'},
	{name: 'Email', url: 'templates/email.html'}];
	$scope.task_type_selected = $scope.task_type[0];
	
	
	$scope.users = [];
	$scope.projects = [];
	
	$scope.empty_rows = [];
	$scope.empty_rows.length = 10;
	
	$scope.tab_name = "Tasks";
	var previous_id = null;
	var previous_td_color = null;
	///////////////
	$scope.addText = function(){
		addText($scope.textdata);
	}
	$scope.verify = function()
	{
		verify();
	}
	$scope.DeleteStep = function()
	{
		DeleteStep();
		$scope.curr_selected=null;
		$scope.toggleFormDisable();
	}
	$scope.addNewStep = function(type)
	{
		if(type == "simple_step")
		{
			var stepname = "Step " + stepcount;
			var in_graph_id = addProcess(stepname);
			var description = "This is a simple step";
			var newstep = new newStep(stepcount, graph_id, stepname, description, type, in_graph_id);
			tasks[newstep.id] = [];
		}
		
		else if(type == "md_step")
		{
			var stepname = "D " + stepcount;
			var in_graph_id = addMDstep(stepname);
			var description = "This is a manual decision step";
			var newstep = new newStep(stepcount, graph_id, stepname, description, type, in_graph_id);
			options[newstep.id] = [];
		}
		
		else if(type == "ad_step")
		{
			var stepname = "AD " + stepcount;
			var in_graph_id = addADstep(stepname);
			var description = "This is a automated decision step";
			var newstep = new newStep(stepcount, graph_id, stepname, description, type, in_graph_id);
			options[newstep.id] = [];
		}
		else if(type == "parallel_step")
		{
			//var stepname = "AD " + stepcount;
			var in_graph_id = add_parallel($scope.parallel_lanes);
			var description = "This is a parallel step";
			//var newstep = new newStep(stepcount, graph_id, stepname, description, type, in_graph_id);
			//options[newstep.id] = [];			
		}
		steps.push(newstep);
		stepcount += 1;
	};
	
	$scope.saveChangesToSteps = function()
	{
		for(i=0;i<steps.length;i++)
		{
			if($scope.step.id == steps[i].id)
			{
				steps[i] = $scope.step;
				graph.getCell($scope.step.in_graph_id).attributes.attrs.text.text = $scope.step.stepname;
				alert(graph.getCell($scope.step.in_graph_id).attributes.attrs.text.text);
				break;
			}
			
		}
	};
		
    function setEmptyRows(num)
	{
		if(num > 10)
		{
			$scope.empty_rows.length = 0;
		}
		else
		{
			$scope.empty_rows.length = 10-num;
		}
	}
	
    $scope.toggleFormDisable = function()
    {
		var table = document.getElementById("table_disable");
		if(state==1 && curr_selected)
		{
			if(rects[curr_selected.id] == 1 || dsteps[curr_selected.id] == 1)
			{
				for(i=0;i<steps.length;i++)
				{
					if(steps[i].in_graph_id == curr_selected.id)
					{		
						$scope.step = steps[i];
						if(rects[curr_selected.id] == 1)
						{
							$scope.curr_tasks = tasks[steps[i].id];
							setEmptyRows(tasks[steps[i].id].length);
							$scope.tab_name = "Tasks";	
							$scope.tab_url = "templates/tasks_tab.html";
						}
						else if(dsteps[curr_selected.id] == 1)
						{
							$scope.curr_options = options[steps[i].id];
							setEmptyRows(options[steps[i].id].length);
							$scope.tab_name = "Options";
							$scope.tab_url = "templates/options_tab.html";
						}
						break;
					}
				}
				$scope.formDisable = false;
				document.getElementById("disable_tab").style.pointerEvents="auto";
			}
		}
		
		else if(state==0)
		{
			$scope.formDisable = true;
			document.getElementById("disable_tab").style.pointerEvents="none";
			$scope.step = null;
			curr_selected = null;
			var previous_td = document.getElementById("task"+previous_id);
			if(previous_td)
			{
				previous_td.style.backgroundColor = previous_td_color;
				previous_td.style.color = previous_td_text_color;
			}
			var previous_td = document.getElementById("option"+previous_id);
			if(previous_td)
			{
				previous_td.style.backgroundColor = previous_td_color;
				previous_td.style.color = previous_td_text_color;
			}
			previous_id = null;
			previous_td_color = null;
			previous_td_text_color = null;
		}
	};
	
	$scope.getTaskData = function(id)
	{
		for(i=0;i<$scope.curr_tasks.length;i++)
		{
			if($scope.curr_tasks[i].id == id)
			{
				$scope.task = $scope.curr_tasks[i];
				break;
			}
		}
		
		if(previous_id)
		{
			var previous_td = document.getElementById("task"+previous_id);
			previous_td.style.backgroundColor = previous_td_color;
			previous_td.style.color = previous_td_text_color;
		}
		var td = document.getElementById("task"+id);
		previous_id = id;
		previous_td_color = td.style.backgroundColor;
		previous_td_text_color = td.style.color;
		td.style.backgroundColor = "#34495E";
		td.style.color = "#FFF";
	};

	$scope.getOptionData = function(id)
	{
		for(i=0;i<$scope.curr_options.length;i++)
		{
			if($scope.curr_options[i].id == id)
			{
				$scope.option = $scope.curr_options[i];
				break;
			}
		}
		
		if(previous_id)
		{
			var previous_td = document.getElementById("option"+previous_id);
			previous_td.style.backgroundColor = previous_td_color;
			previous_td.style.color = previous_td_text_color;
		}
		var td = document.getElementById("option"+id);
		previous_id = id;
		previous_td_color = td.style.backgroundColor;
		previous_td_text_color = td.style.color;
		td.style.backgroundColor = "#34495E";
		td.style.color = "#FFF";
	};

	$scope.addNew = function() {
		if($scope.step.type == "simple_step")
		{
			var new_task = new newTask();
			new_task.id = taskcount;
			taskcount += 1;
			new_task.step_id = $scope.step.id;
			new_task.taskname = "new task";
			new_task.description = "This is a new task";
			$scope.curr_tasks.push(new_task);
		}
		else if($scope.step.type == "md_step")
		{
			var new_option = new newOption();
			new_option.id = optioncount;
			optioncount += 1;
			new_option.step_id = $scope.step.id;
			new_option.optionname = "new option";
			new_option.description = "This is a new option";
			$scope.curr_options.push(new_option);
			createNew();
		}
	};
	    
    var check;
	 $scope.graph_to_json = function(){
		//Create json object for graph
		var json_object = graph.toJSON(); 
		$scope.result = JSON.stringify(json_object);
	};

	function json_to_graph (json_object){
		var jsonString = JSON.stringify(json_object);
		graph.fromJSON(JSON.parse(jsonString));
	}
		
	$scope.load_graph = function(){    
		$http.get('mock/workflow.json').success(function(workflow_data) {
			graph_id = workflow_data[0].id;
			graph_json = workflow_data[0].graph_json;
			graph.fromJSON(graph_json);
			
			$http.get('mock/steps.json').success(function(steps_data) {
				for(i=0; i<steps_data.length; i++)
				{
					if(steps_data[i].graph_id == graph_id)
					{
						if(steps_data[i].type == "simple_step")
						{
							tasks[steps_data[i].id] = [];
							rects[steps_data[i].in_graph_id] = 1;
						}
						
						else if(steps_data[i].type == "d_step")
						{
							options[steps_data[i].id]=[];
							dsteps[steps_data[i].in_graph_id] = 1;
						}
						steps.push(steps_data[i]);
					}
				}
			});
						
			$http.get('mock/tasks.json').success(function(tasks_data) {
				for(i=0; i<steps.length; i++)
				{
					for(j=0; j<tasks_data.length; j++)
					{
						if(tasks_data[j].step_id == steps[i].id)
						{
							tasks[steps[i].id].push(tasks_data[j]);
						}
					}
				}
			});
			
			$http.get('mock/options.json').success(function(options_data) {
				for(i=0; i<steps.length; i++)
				{
					for(j=0; j<options_data.length; j++)
					{
						if(options_data[j].step_id == steps[i].id)
						{
							options[steps[i].id].push(options_data[j]);
						}
					}
				}
			});
		});
						
/*		$http.get('mock/users.json').success(function(data) {
			$scope.users = data;
		});
		
		$http.get('mock/scope.json').success(function(data) {
			$scope.projects = data;
		});*/
	};
   
}]);

