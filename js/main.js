(function () {
  var app = angular.module('Field', ['ngResource']);

   app.factory('FieldService', ['$resource', function($resource){
    return $resource('mocks/data.json/:id', {id: '@m_id'});

    //use if you want to get a single object in a json file that has the path 'mocks/fields/{id}'
    //return $resource('mocks/fields/:id', {id: '@m_id'});
  }]);

  app.factory('FieldService1', ['$resource', function($resource){
    return $resource('mocks/fields/:id', {id: '@m_id'});
  }]);

  app.factory('SelectedFieldService', ['FieldService1', function(FieldService1) {
    var selectedField = {};

    selectedField.selectedId = '';
    selectedField.fieldDef = {};

    selectedField.selectField = function() {
      this.fieldDef = FieldService1.get({id: this.selectedId});
    };

    selectedField.changeSelectedId = function(newId) {
      this.selectedId = newId;
    };

    selectedField.saveField = function() {
      FieldService1.save({id: this.selectedId}, this.fieldDef);
    }

    return selectedField;

  }]);
  
  app.directive('fieldPanel', function(){
    return {
      restrict: 'E',
      templateUrl: 'field-panel.html',
      controller: ['SelectedFieldService', function(SelectedFieldService) {
        this.tab = 1;

        this.numTab = 3;
        
        //this.fields = [{name : "Name", description: "Your name", type: "text"}];
        
        this.dataTypes = [{name: "Text", value: 0},
        {name: "Integer", value: 1},
        {name: "Decimal", value: 2},
        {name: "Date", value: 3},
        {name: "Enumeration", value: 4}
        ];

        this.dataSources = [{name: "Manual", value: 0},
        {name: "Calculated", value: 1},
        {name: "Imported", value: 2}
        ];

        this.calcTypes = [{name: "Formula", value: 0},
        {name: "Script", value: 1}
        ];
        
        this.fieldDef = SelectedFieldService.fieldDef;

        this.selectTab = function(setTab) {
          this.tab = setTab;
        };

        this.isSelected = function(checkTab) {
          return this.tab === checkTab;
        };

        this.nextTab = function() {
          this.selectTab(this.tab + 1);
        };

        this.prevTab = function() {
          this.selectTab(this.tab - 1);
        };

        this.isLastTab = function() {
          return this.tab == this.numTab;
        }

        this.isFirstTab = function() {
          return this.tab == 1;
        }

        this.isType = function(type) {
          return SelectedFieldService.fieldDef.m_dataType === type;
        };
      }],
      controllerAs: 'panel'
    };
  });

  app.controller('ListCtrl', function() {
    this.list = [{label: "label", value: 1}, {label: "label1", value: 2}];

    this.addValue = function() {
    this.list.push({label:"label", value: "1"});
    };

    this.removeValue = function(valueToRemove) {
      var index = this.list.indexOf(valueToRemove);
      this.list.splice(index, 1);
    };

    this.clearValue = function(value) {
      value.label = '';
      value.value = '';
    };
  });

  app.controller("FieldIndexCtrl", ['FieldService', function(FieldService) {
    this.fields = FieldService.query();

    //use it to get a single object
    //this.field1 = FieldService.get({id: 1});

  }]); 

  app.directive('listPanel', function(){
    return {
      restrict: 'E',
      templateUrl: 'list-panel.html',
      controller: ['FieldService', 'SelectedFieldService', function(FieldService, SelectedFieldService) {
        this.fieldList = FieldService.query();
        //this.field1 = FieldService.get({id: 1});

        this.tab = 1;
        this.selectedField = {};
        this.selectTab = function(setTab) {
          this.tab = setTab;
        };

        this.isSelected = function(checkTab) {
          return this.tab === checkTab;
        };

        this.selectField = function(field) {
          this.selectedField = field;
          SelectedFieldService.changeSelectedId(field.m_id);
        };

        this.isSelectedField = function(field) {
          return this.selectedField === field;
        };

        this.getSelectedField = function() {
          return this.selectedField;
        };

      }],
      controllerAs: 'list'
    };
  }); 

  app.controller("MainPanelCtrl", ['SelectedFieldService', function(SelectedFieldService) {
    this.editField = function() {
      SelectedFieldService.selectField();
      this.fieldDef = SelectedFieldService.fieldDef;
    };

    this.newField = function() {
      SelectedFieldService.fieldDef = {};
      this.fieldDef = SelectedFieldService.fieldDef;
    };

    this.saveField = function() {
      SelectedFieldService.fieldDef = this.fieldDef;
      SelectedFieldService.saveField();
    };

    this.editScript = function() {
      document.getElementById('scriptPopUp').style.display='block';
      document.getElementById('fade').style.display='block';
    };

    this.closePopUp = function() {
      document.getElementById('scriptPopUp').style.display='none';
      document.getElementById('fade').style.display='none';
    };
  }]);

  app.controller("ScriptEditorCtrl", ['FieldService', function(FieldService) {
    this.mathFunc = ["+", "-", "*", "/", "(", ")"];
    this.fieldList = FieldService.query();

    this.selectField = function(field) {
      this.selectedField = field;
      SelectedFieldService.changeSelectedId(field.m_id);
    };

    this.isSelectedField = function(field) {
      return this.selectedField === field;
    };

    this.add = function(value) {
      document.getElementById('script').innerHTML += value;
    };
  }]);

}) ();
