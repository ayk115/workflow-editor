angular.module('app.controllers', [])
  .controller('editor', ['$scope','AngularIssues','readFormData','$http' , function($scope , AngularIssues, readFormData, $http) {
	///////////////
	$scope.formDisable = true;
	var graph_id = null;
	var graph_json = null;
	var curr_selected = null;

	var steps = [];
	$scope.step = null;
	var stepcount = 10;
	function newStep()
	{
		var id;
		var graph_id;
		var stepname;
		var description;
		var type;
		var in_graph_id;
	}
	
	var tasks = {};
	$scope.curr_tasks = [];
	$scope.task = null;
	$scope.curr_task_desc = null;
	$scope.show_tasks = null;
	var taskcount = 10;
	function newTask()
	{
		var id;
		var taskname;
		var step_id;
		var description;
	}
	
	var options = {};
	$scope.curr_options = [];
	$scope.option = null;
	$scope.curr_option_desc = null;
	$scope.show_options = null;
	var optioncount = 10;
	function newOption()
	{
		var id;
		var optionname;
		var step_id;
		var description;
	}
	
	$scope.users = [];
	$scope.projects = [];
	
	$scope.empty_rows = [];
	$scope.empty_rows.length = 10;
	
	$scope.tab_name = "Tasks";
	var previous_id = null;
	var previous_td_color = null;
	///////////////
	/////////////////
	var divWidth=$("#Div").width();
    var graph = new joint.dia.Graph;
    var w=divWidth;
    var h=600;
    var paper = new joint.dia.Paper({
        el: $('#myholder'),
        width: w,
        height:h,
        model: graph,
        gridsize:4
    });
    var paper2 = new joint.dia.Paper({
        el: $('#myimage'),
        width: w,
        height:600,
        model: graph,
        interactive : false
    });

    var rect_w=70,rect_h=30;
    var rects=new Object();
    var texts=[];
    var cnt = 1;
    var dsteps =new Object();
    var dsteps_child =new Object();
    var asteps =new Object();
    var dcnt = 0;
    var parallel_inner_option = new Object();
    var parallel_inner_step = new Object();
    var parallel_outer = new Object;
    var links = [];
    var vert = new Object();
	var state = 0;
    var click_id;
    var pos_x;
    var pos_y;


    var start = new joint.shapes.basic.Circle({
        position:{ x:w/4,y:h/10},
        attrs:{circle:{fill:'black'}, text : {text:'Start',fill:'white'}}
    });
    var end = new joint.shapes.basic.Circle({
        position:{ x:(w*6)/15,y:(4*h)/5},
        attrs:{circle:{fill:'black'}, text : {text:'End',fill:'white'}}
    });
    graph.addCells([start,end]);


     // = AngularIssues.get({},{'id':1});

    $scope.toggleFormDisable = function()
    {
		alert("toggle");
		var table = document.getElementById("table_disable");
		if(state==1 && curr_selected)
		{
			if(rects[curr_selected.id] == 1 || dsteps[curr_selected.id] == 1)
			{
				for(i=0;i<steps.length;i++)
				{
					if(steps[i].in_graph_id == curr_selected.id)
					{		
						$scope.step = steps[i];
						if(rects[curr_selected.id] == 1)
						{
							$scope.curr_tasks = tasks[steps[i].id];
							setEmptyRows(tasks[steps[i].id].length);
							$scope.tab_name = "Tasks";	
							$scope.show_tasks = true;
							$scope.show_options = false;
						}
						else if(dsteps[curr_selected.id] == 1)
						{
							$scope.curr_options = options[steps[i].id];
							setEmptyRows(options[steps[i].id].length);
							$scope.tab_name = "Options";
							$scope.show_tasks = false;
							$scope.show_options = true;
						}
						break;
					}
				}
				$scope.formDisable = false;
				document.getElementById("disable_tab").style.pointerEvents="auto";
			}
		}
		
		else if(state==0)
		{
			$scope.formDisable = true;
			document.getElementById("disable_tab").style.pointerEvents="none";
			$scope.step = null;
			curr_selected = null;
			var previous_td = document.getElementById("task"+previous_id);
			if(previous_td)
			{
				previous_td.style.backgroundColor = previous_td_color;
				previous_td.style.color = previous_td_text_color;
			}
			var previous_td = document.getElementById("option"+previous_id);
			if(previous_td)
			{
				previous_td.style.backgroundColor = previous_td_color;
				previous_td.style.color = previous_td_text_color;
			}
			previous_id = null;
			previous_td_color = null;
			previous_td_text_color = null;
		}
	};
	
	$scope.getTaskData = function(id)
	{
		for(i=0;i<$scope.curr_tasks.length;i++)
		{
			if($scope.curr_tasks[i].id == id)
			{
				$scope.task = $scope.curr_tasks[i];
				break;
			}
		}
		
		if(previous_id)
		{
			var previous_td = document.getElementById("task"+previous_id);
			previous_td.style.backgroundColor = previous_td_color;
			previous_td.style.color = previous_td_text_color;
		}
		var td = document.getElementById("task"+id);
		previous_id = id;
		previous_td_color = td.style.backgroundColor;
		previous_td_text_color = td.style.color;
		td.style.backgroundColor = "#34495E";
		td.style.color = "#FFF";
	};

	$scope.getOptionData = function(id)
	{
		for(i=0;i<$scope.curr_options.length;i++)
		{
			if($scope.curr_options[i].id == id)
			{
				$scope.option = $scope.curr_options[i];
				break;
			}
		}
		
		if(previous_id)
		{
			var previous_td = document.getElementById("option"+previous_id);
			previous_td.style.backgroundColor = previous_td_color;
			previous_td.style.color = previous_td_text_color;
		}
		var td = document.getElementById("option"+id);
		previous_id = id;
		previous_td_color = td.style.backgroundColor;
		previous_td_text_color = td.style.color;
		td.style.backgroundColor = "#34495E";
		td.style.color = "#FFF";
	};

	$scope.addNew = function() {
		if($scope.step.type == "simple_step")
		{
			var new_task = new newTask();
			new_task.id = taskcount;
			taskcount += 1;
			new_task.step_id = $scope.step.id;
			new_task.taskname = "new task";
			new_task.description = "This is a new task";
			$scope.curr_tasks.push(new_task);
		}
		else if($scope.step.type == "d_step")
		{
			var new_option = new newOption();
			new_option.id = optioncount;
			optioncount += 1;
			new_option.step_id = $scope.step.id;
			new_option.optionname = "new option";
			new_option.description = "This is a new option";
			$scope.curr_options.push(new_option);
			$scope.createNew();
		}
	};
	

    //Add decision step to graph;
    $scope.addMDstep = function() {
        dcnt+=1;
        var rect = new joint.shapes.basic.Path({
            size: { width: 70, height:70 },
            attrs: {
                path: { d: 'M 30 0 L 60 30 30 60 0 30 z' ,fill : '#9FA4EB'},
                text: {
                    text: 'D' + dcnt,
                    'ref-y': .5 // basic.Path text is originally positioned under the element
                }
            }
        });
        
        var mdstep = new newStep();
        mdstep.id = stepcount;
        mdstep.stepname = 'D'+stepcount;
        stepcount += 1;
        mdstep.type = "d_step";
        mdstep.graph_id = 1;
        mdstep.in_graph_id = rect.id;
        mdstep.description = "This is decision step";
        steps.push(mdstep);
        options[mdstep.id] = [];
        dsteps[rect.id]=1;
        graph.addCells([rect]);
    };

    $scope.DeleteStep = function(){
        // alert(state);
        if(state==1  && click_id.id!=start.id && click_id.id!=end.id && dsteps_child[click_id.id]!=1)
        {
            click_id.remove();
            dsteps[click_id.id]=0;
            parallel_outer[click_id.id]=0;
            rects[click_id.id]=0;
        }
    };

    $scope.addADstep = function() {
        dcnt+=1;
        var rect = new joint.shapes.basic.Path({
            size: { width: 70, height:70 },
            attrs: {
                path: { d: 'M 30 0 L 60 30 30 60 0 30 z' ,fill : '#4369E5'},
                text: {
                    text: 'A' + dcnt,
                    'ref-y': .5 // basic.Path text is originally positioned under the element
                }
            }
        });
        var mdstep = new step();
        mdstep.id = stepcount;
        mdstep.stepname = 'D'+stepcount;
        stepcount += 1;
        mdstep.type = "d_step";
        mdstep.graph_id = 1;
        mdstep.in_graph_id = rect.id;
        mdstep.description = "This is decision step";
        steps.push(mdstep);

        asteps[rect.id]=1;
        graph.addCells([rect]);
    };

    function rebuild_parallel(lane,outer_box)
    {
            var x1 = outer_box.get('position').x;
            var y1 = outer_box.get('position').y;
            var first_child = outer_box.get('embeds');
            var incoming = graph.getConnectedLinks(outer_box, { outbound: true });
            var outgoing = graph.getConnectedLinks(outer_box, { inbound: true });
            var ln_cnt=0;
            var mx=0;
            for(var i=0;i<first_child.length;i++)
            {
                if(!vert[first_child[i]])
                {
                    ln_cnt+=1;
                    var stp_len = graph.getCell(first_child[i]).get('embeds');//.length;
                    if(parseInt(stp_len.length)>mx)
                    {
                         mx=parseInt(stp_len.length);
                    }   
                }
            }
            mx+=2;
            var h= (((parseInt(mx)+1))*parseInt(rect_h));//*w)/1500;
            var w = (((parseInt(ln_cnt))*rect_w+parseInt(10*parseInt(ln_cnt))));//*h)/500;
            var rect = new joint.shapes.basic.Rect({
                    position: { x: x1, y: y1 },
                    size: { width: w, height: h },
                    attrs: { rect: { }, text: {} }
            });
            var lane_ar = new Object();
            var k=0;
            for(var i=0;i<first_child.length;i++)
            {
                if(!vert[first_child[i]])
                {
                    var stp=graph.getCell(first_child[i]).get('embeds');
                    lane_ar[k]=parseInt(stp.length);
                    if(first_child[i]==lane.id)
                    {
                        lane_ar[k]+=2;//stp.length+1;
                    }
                    k++;
                }
            }
            graph.addCells([rect]); 
            parallel_outer[rect.id]=1;
            outer_box.remove();
            if(incoming.length==1)
            {
                var src=incoming[0].get('target');
                //alert(src.id);
                var ln = new joint.dia.Link({
                    source: { id: rect.id  }, target: { id: src.id},
                    attrs: { '.marker-source': { d: 'M 10 0 L 0 5 L 10 10 z' } }
                });
                links.push([ln.id,src.id,rect.id]);
                graph.addCell([ln]);
            }
            if(outgoing.length==1)
            {
                var trgt = outgoing[0].get('source');
                var ln = new joint.dia.Link({
                    source: { id: trgt.id }, target: { id: rect.id },
                    attrs: { '.marker-source': { d: 'M 10 0 L 0 5 L 10 10 z' } }
                });
                links.push([ln.id,rect.id,trgt]);
                graph.addCell([ln]);
            }
            var prev=x1;
            //alert(k);
            for(var i=0;i<k;i++)
            {
                var rect2;
                //alert(lane_ar[k]);
                if(i>0)
                {
                    rect2 = new joint.shapes.basic.Rect({
                        position: { x: parseInt(prev)+4, y: y1 },
                        size: { width: rect_w, height: h },
                        attrs: { rect: { fill: '#3A9CF2' }, text: {} }
                    });
                    var ln = new joint.shapes.basic.Rect({
                        position: { x: parseInt(prev), y: y1 },
                        size: { width: 2, height: h },
                        attrs: { rect : { fill: '#3A9CF2'}, text: {} }
                    });
                    rect.embed(ln);
                    graph.addCells([ln]);
                    vert[ln.id]=1;
                    paper.findViewByModel(ln).options.interactive = false; 
                }
                else
                {
                    rect2 = new joint.shapes.basic.Rect({
                        position: { x: parseInt(prev)+4, y: y1 },
                        size: { width: rect_w, height: h },
                        attrs: { rect: { fill: '#3A9CF2' }, text: {} }
                    });
                }
                rect.embed(rect2);
                graph.addCells([rect2]);
                rect2.toBack();
                //alert(lane_ar[i]);
                var st_y=y1;
                for(var j=1;j<=lane_ar[i];j++)
                {
                    if(j%2==1)
                    {
                        var click_rect = new joint.shapes.basic.Rect({
                            position: { x: parseInt(prev)+(rect_w)/2, y: st_y+(rect_h)/2 },
                            size: { width: 10, height: 10 },
                            attrs: { rect: { fill:  '#3A9CF2'  }, text: {} }
                        });
                        st_y+=(rect_h/2+10);
                        graph.addCells([click_rect]);
                        click_rect.toFront();
                        rect2.embed(click_rect);
                        paper.findViewByModel(click_rect).options.interactive = false; 
                        parallel_inner_option[click_rect.id]=1;
                    }
                    else
                    {
                        var step_rect = new joint.shapes.basic.Rect({
                            position: { x: parseInt(prev)+5, y: st_y+(rect_h)/2 },
                            size: { width: rect_w , height: rect_h },
                            attrs: { rect: { fill:  '#3A9CF2'  }, text: {} }
                        });
                        st_y+=(rect_h/2+rect_h);
                        rect2.embed(step_rect);
                        graph.addCells([step_rect]);
                        step_rect.toFront();
                        parallel_inner_step[step_rect.id]=1;
                        paper.findViewByModel(step_rect).options.interactive = false; 
                    }
                }
                prev=parseInt(prev)+10+parseInt(rect_w);
                paper.findViewByModel(rect2).options.interactive = false; 
                //Build the lanes
                //For each line build the steps;
            };
            setColor(0,click_id);
            state=0;
    }
    //Add simple step to graph;
    $scope.addProcess=function () {
        if(state==1)
        {
            cur_lane = graph.getCell(click_id.get('parent'));
            out_box = graph.getCell(cur_lane.get('parent'));
            rebuild_parallel(cur_lane,out_box);
        }
        else
        {
            var rect = new joint.shapes.basic.Rect({
                position: { x: (100*w)/1500, y: (30*h)/500 },
                size: { width: rect_w, height:rect_h },
                attrs: { rect: { fill: '#3A9CF2',rx: 15, ry: 15}, text: { text: 'Step' + cnt, fill: 'white' } }
            });
            cnt+=1;
            rects[rect.id]=1;
            graph.addCells([rect]);
        }
    };

    $scope.removeProcess = function(){
        rects[$scope.rprocess].remove();
    };
    $scope.removeText = function(){
        texts[$scope.rtext].remove();
    };
    $scope.addText = function(){
        var txt = new joint.shapes.basic.Text({
            position: {x:(100*w)/1500,y:(30*h)/500},
            size: { width:rect_w , height:rect_h},
            attrs: {text: {text: $scope.textdata }}
        });
        texts.push(txt);
        graph.addCells([txt]);
    }

    //Checking if link already exists in id1 and id2;
    function check_link(cell1,cell2){
        var fl=1;
        var allLinks=graph.getLinks();
        for (var i = 0; i <allLinks.length; i++) {
            for (var j=0; j < links.length; j++) { 
                if(links[j][1] == cell1.id && links[j][2] == cell2.id && links[j][0]==allLinks[i].id)
                {
                    fl=0;
                    break;
                }
            };
            if(fl==0)
            {
                break;
            }
        };
        return fl;
    }

    //Setting color of the current cell as selected;
    function setColor(par,cell){
            if(par && !vert[cell.id])
            {
                cell.attr({
                    rect:{fill:'white'},
                    circle:{fill:'black'},
                    path:{fill:'#9FA4EB'},
                    text: { fill: 'white', 'font-size': 15 }
                });   
            }
            else
            {
                cell.attr({
                 rect:{fill:'#3A9CF2'},
                 circle:{fill:'black'},
                 text: { fill: 'white', 'font-size': 15 }
                });          
                if(asteps[cell.id])
                {
                    cell.attr({path:{fill:'#4369E5'}});
                }
                if(dsteps[cell.id])
                {
                    cell.attr({path:{fill:'#9FA4EB'}});
                }
                if(vert[cell.id])
                {
                    cell.attr({rect:{fill:'black'}});
                }
            }  
    }
    //Join two cells 
    function onclick_join(cellView,ect,x,y) {
        //If state is zero then first block 
         if(state==0)
        {
            cellView.attr({
                rect:{fill:'#E67E22'},
                circle:{fill:'#E67E22'},
                path:{fill:'#E67E22'},
                text: { fill: 'white', 'font-size': 15 }
            });
            paper.$el.addClass('connecting');
            click_id=cellView;
            pos_x=x;
            pos_y=y;
            state=1;
        }
        //Now join the previously selected cell with current cell
        else
        {
             if(parallel_inner_option[click_id.id] || parallel_inner_step[click_id.id])
             {
                
             }
            //If previously selected cell is not same as current one
            else if(cellView.id!=click_id.id )
            {
                 var dec = 0;                

                 //Checking incoming for the decision step;A decision step should have only one 
                 if(dsteps[cellView.id])
                 {
                    var incoming = graph.getConnectedLinks(cellView, { outbound: true });        
                    if(incoming.length==1)
                    {
                        dec=1;
                    }
                 }
                 //If previously clicked cell is decision step then It can not be linked with any other step
                 //If current cell is child of decision step
                 if (dsteps[click_id.id] || dsteps_child[cellView.id])
                 {
                        dec = 1;
                 }
                 //If previously clicked cell is child of decision step
                 if(dsteps_child[click_id.id])
                 {
                    var outgoing = graph.getConnectedLinks(click_id, { inbound: true });        
                    if(outgoing.length==1)
                    {
                        dec=1;
                    }                        
                 }
                 if(parallel_outer[cellView.id])
                 {
                    var incoming = graph.getConnectedLinks(cellView, { outbound: true });        
                    if(incoming.length==1)
                    {
                        dec=1;
                    }
                 }
                 if (parallel_outer[click_id.id])
                 {
                    var outgoing = graph.getConnectedLinks(click_id, { inbound: true });        
                    if(outgoing.length==1)
                    {
                        dec = 1;
                    }
                 }
                 //A normal step can't have more than one incoming;
                 if(rects[cellView.id])
                 {
                    var incoming = graph.getConnectedLinks(cellView, { outbound: true });        
                    if(incoming.length==1)
                    {
                        dec=1;
                    }
                 }
                 //A normail step can't have more than one outgoing;
                 if (rects[click_id.id])
                 {
                    var outgoing = graph.getConnectedLinks(click_id, { inbound: true });        
                    if(outgoing.length==1)
                    {
                        dec = 1;
                    }
                 }
                 if(dec==1 || cellView.id==start.id)
                 {

                 }
                 else if(check_link(cellView,click_id))
                 {                    
                    var ln = new joint.dia.Link({
                        source: { id: cellView.id }, target: { id: click_id.id },
                        attrs: { '.marker-source': { d: 'M 10 0 L 0 5 L 10 10 z' } }
                    });
                    links.push([ln.id,cellView.id,click_id.id]);
                    graph.addCell([ln]);
                 }
                if(parallel_outer[click_id.id])
                {
                    setColor(1,click_id);
                }
                else
                {
                    setColor(0,click_id);
                }
                if(parallel_outer[cellView.id])
                {
                    setColor(1,cellView);
                }
                else
                {
                    setColor(0,cellView);
                }
                paper.$el.removeClass('connecting');
                state=0;
            }
        }           
    }

    function ondrag_join (cellView,evt,x,y) {
        var elementBelow = graph.get('cells').find(function(cell) {
            if (cell instanceof joint.dia.Link) return false; // Not interested in links.
            if (cell.id === cellView.model.id) return false; // The same element as the dropped one.
            if (cell.getBBox().containsPoint(g.point(x, y))) {
                return true;
            }
            return false;
        });

        if (elementBelow && !_.contains(graph.getNeighbors(elementBelow), cellView.model)) {
        graph.addCell(new joint.dia.Link({
            source: { id: cellView.model.id }, target: { id: elementBelow.id },
            attrs: { '.marker-source': { d: 'M 10 0 L 0 5 L 10 10 z' } }
        }));
            // Move the element a bit to the side.
            cellView.model.translate(-200, 0);
        }
    }
    function add_rect(x_val,y_val){
        var rect = new joint.shapes.basic.Rect({
                position: { x: x_val, y: y_val },
                size: { width: rect_w, height: rect_h },
                attrs: { rect: { fill: '#3A9CF2',rx: 15, ry: 15}, text: {} }
        });
        graph.addCells([rect]);
    }
    //$scope.draw_line =function  (){
    function draw_line(cell,x,y){
        var mo=graph.findModelsFromPoint(g.point(x,y));
        var all_el = graph.getElements();
       // alert('asdf');
        for (var i = 0; i < all_el.length; i++) {
            if(parallel_outer[all_el[i].id])
            {
                var emb = (all_el[i].get('embeds'));
                var cnt=0;
                for (var j = 0; j <emb.length ;  j++) {
                    if(!vert[emb[j]] && mo[0].id==emb[j])
                    {
                        var cl=graph.getCell(emb[j]);
                        return cl;
                    }
                };
            }
        };
        return 0;
    }

    var out_h,out_w;
    function outer_box(x_val,y_val,steps){
        //alert(steps);
        //out_h= (((parseInt($scope.parallel_steps)+1))*parseInt(rect_h));
        out_h=rect_h;
        out_w = (((parseInt(steps))*rect_w+parseInt(10*parseInt(steps))));
        var rect = new joint.shapes.basic.Rect({
                position: { x: x_val, y: y_val },
                size: { width: out_w, height: out_h },
                attrs: { rect: { }, text: {} }
        });
        graph.addCells([rect]); 
        parallel_outer[rect.id]=1;
        var prev=x_val;
        for (var i = 0; i < (parseInt(steps)); i++) {
            var rect2;
            if(i>0)
            {
                rect2 = new joint.shapes.basic.Rect({
                    position: { x: parseInt(prev)+10, y: y_val },
                    size: { width: rect_w, height: out_h },
                    attrs: { rect: { fill: '' }, text: {} }
                });
                var ln = new joint.shapes.basic.Rect({
                    position: { x: parseInt(prev)+5, y: y_val },
                    size: { width: 2, height: out_h },
                    attrs: { rect : { fill: 'black'}, text: {} }
                }); 
                rect.embed(ln);
                graph.addCells([ln]);
                vert[ln.id]=1;
                paper.findViewByModel(ln).options.interactive = false; 
            }
            else
            {
                rect2 = new joint.shapes.basic.Rect({
                    position: { x: parseInt(prev)+5, y: y_val },
                    size: { width: rect_w, height: out_h },
                    attrs: { rect: { fill: '' }, text: {} }
                });
            }
            rect3 = new joint.shapes.basic.Rect({
                position: { x: parseInt(prev)+(rect_w)/2, y: y_val +(rect_h)/2 },
                size: { width: 10, height: 10 },
                attrs: { rect: { fill: '#3A9CF2' }, text: {} }
            });
            prev=parseInt(prev)+10+parseInt(rect_w);
            rect.embed(rect2);
            rect2.embed(rect3);
            rect3.toFront();
            graph.addCells([rect2,rect3]);
            parallel_inner_option[rect3.id]=1;
            rect2.toBack();    
            paper.findViewByModel(rect2).options.interactive = false; 
            paper.findViewByModel(rect3).options.interactive = false; 
        };
        //cnt+=1;
    }
    function is_child(cellView){
        var all = graph.getElements();
        for (var i =0;i < all.length ; i++) {
            if(parallel_outer[all[i].id]==1)
            {
                var emb = (all[i].get('embeds'));
                for(var j=0 ;j<emb.length ;j++ )
                {
                    if(cellView.model.id==emb[j])
                    {
                        return 0;
                    }
                }
            }
        };
        return 1;
    }

    $scope.add_parallel = function (){
        var steps = $scope.parallel_lanes;
        var st_x = 100;st_y=100;
        if(steps==0)
        {
            alert('Atleast one step is required!!!');
        }
        else
        {
            outer_box((st_x*w)/1500,(st_y*h)/500,steps);
        }
    }

    $scope.verify =function (){
        //Start step should have an outgoing link
        var inboundStart = graph.getConnectedLinks(start, { inbound: true });
        var outboundEnd = graph.getConnectedLinks(end, { outbound: true });
        var all_el = graph.getElements();
        var flag=0;
        for (var i = 0; i <all_el.length; i++) {
                if(rects[all_el[i].id])
                {
                    var incoming = graph.getConnectedLinks(all_el[i], { outbound: true });        
                    var outgoing = graph.getConnectedLinks(all_el[i], { inbound: true });        
                    if(!(incoming.length==1 && outgoing.length==1))
                    {
                        flag=1;
                        alert("Error from normal step!!!");
                        break;
                    }
                }
                else if(parallel_outer[all_el[i].id])
                {
                    var incoming = graph.getConnectedLinks(all_el[i], { outbound: true });        
                    var outgoing = graph.getConnectedLinks(all_el[i], { inbound: true });        
                    if(!(incoming.length==1 && outgoing.length==1))
                    {
                        flag=1;
                        alert("Error from parallel step!!!");
                        break;
                    }
                }
                else if(dsteps[all_el[i].id])
                {
                    var incoming = graph.getConnectedLinks(all_el[i], { outbound: true });        
                    var outgoing = graph.getConnectedLinks(all_el[i], { inbound: true });        
                    if(!(incoming.length==1 && outgoing.length==2))
                    {
                        flag=1;
                        alert("Error from decision step!!!");
                        break;
                    }
                }
        };
        if(flag==1)
        {
 
        }
        else if(inboundStart.length!=1)
        {
            alert("Error from start");
        }
        else if(outboundEnd.length==0)
        {
            alert("No link to end");
        }
        else
        {
            alert("No Error");
        }
    }
    //We can have pointerup but that will not be optimal
    paper.on('cell:pointerclick', function(cellView, evt, x, y) {
		alert("pointerclick");
       // alert(cellView.model.id);
        //alert(cellView.model.get('embeds'));
        //alert(graph.getCell(cellView.model.get('parent')).get('parent'))
        //var mo=graph.findModelsFromPoint(g.point(x,y));
        //var ret;
        /*if(mo.length==2)
        {
            ret=mo[1];
           // ret=mo[mo.length-1];        
        }
        else if(mo.length>2)
        {
            ret=mo[1];
        }
        else
        {*/
        ret=cellView.model;
        //}

		curr_selected=ret;

        if (ret)
        {
            if(!vert[ret.id])
            {
				//alert(ret.id);
                onclick_join(ret,evt,x,y);
            }
        }
        else//Here we can join with the parent
        {
            paper.$el.removeClass('connecting');
            state = 0;
        }
  
    });

    paper.on('cell:pointerup',function(cellView, evt, x, y){
      
    });
    paper.on('blank:pointerclick', function(evt, x, y) {
           all_el = graph.getElements();
           for (var i = 0; i <all_el.length; i++) {
                if (parallel_outer[all_el[i].id]) 
                {
                    setColor(1,all_el[i]);
                }
                else
                {
                    setColor(0,all_el[i]);
                }
            }; 
           paper.$el.removeClass('connecting');
           state=0;
    });

    
    var check;
     $scope.graph_to_json = function(){
        //Create json object for graph
        var json_object = graph.toJSON(); 
        $scope.result = JSON.stringify(json_object);
    }
    function json_to_graph (json_object){
        var jsonString = JSON.stringify(json_object);
        graph.fromJSON(JSON.parse(jsonString));
    }
		
    $scope.load_graph = function(){    
        $http.get('mock/workflow.json').success(function(workflow_data) {
			graph_id = workflow_data[0].id;
			graph.fromJSON(workflow_data[0].graph_json);
			
	        $http.get('mock/steps.json').success(function(steps_data) {
				for(i=0; i<steps_data.length; i++)
				{
					if(steps_data[i].graph_id == graph_id)
					{
						tasks[steps_data[i].id] = [];	
						if(steps_data[i].type == "simple_step")
						{
							rects[steps_data[i].in_graph_id] = 1;
						}
						
						else if(steps_data[i].type == "d_step")
						{
							dsteps[steps_data[i].in_graph_id] = 1;
						}
						steps.push(steps_data[i]);
					}
				}
			});
						
			$http.get('mock/tasks.json').success(function(tasks_data) {
				for(i=0; i<steps.length; i++)
				{
					for(j=0; j<tasks_data.length; j++)
					{
						if(tasks_data[j].step_id == steps[i].id)
						{
							tasks[steps[i].id].push(tasks_data[j]);
						}
					}
				}
			});
		});
				
		/*$http.get('mock/tasks.json').success(function(data) {
			$scope.tasks = data;
		});
		
		$http.get('mock/users.json').success(function(data) {
			$scope.users = data;
		});
		
		$http.get('mock/scope.json').success(function(data) {
			$scope.projects = data;
		});*/
    }
    
    $scope.createNew = function(){        
            if(dsteps[curr_selected.id])
            {
                var connected=curr_selected.get('embeds'); 
                var opt = 1;
                if(connected)
                {
                    opt=connected.length+1;
                    for (var i =0 ;i<connected.length;i++) {
                        var cur_cell=graph.getCell(connected[i]);
                        cur_cell.remove();
                    };
                } 
                var middle=parseInt(opt/2);
                for(var i=0;i<middle;i++)
                {
                    var rect = new joint.shapes.basic.Rect({
                        position: { x: parseInt(curr_selected.get('position').x)-(i+1)*30 , y: parseInt(curr_selected.get('position').y)+40+rect_w  },
                        size: { width: 15, height: 15},
                        attrs: { rect: { fill: '#3A9CF2',rx: 15, ry: 15}, text: {} }
                    }); 
                    var ln = new joint.dia.Link({
                        source: { id: rect.id }, target: { id: curr_selected.id },
                         attrs: { '.marker-source': { d: 'M 10 0 L 0 5 L 10 10 z' } }
                    });
                    curr_selected.embed(rect);
                    dsteps_child[rect.id]=1;
                    graph.addCells([rect,ln]);   
                }
                for(var i=middle+1;i<=opt;i++)
                {
                    var rect = new joint.shapes.basic.Rect({
                        position: { x: parseInt(curr_selected.get('position').x)+(i+1-middle)*30 , y: parseInt(curr_selected.get('position').y)+40+rect_w  },
                        size: { width: 15, height: 15},
                        attrs: { rect: { fill: '#3A9CF2',rx: 15, ry: 15}, text: {} }
                    }); 
                    var ln = new joint.dia.Link({
                        source: { id: rect.id }, target: { id: curr_selected.id },
                         attrs: { '.marker-source': { d: 'M 10 0 L 0 5 L 10 10 z' } }
                    });
                    curr_selected.embed(rect);
                    dsteps_child[rect.id]=1;
                    graph.addCells([rect,ln]); 
                    //alert(i);   
                }
            }
    }
}]);


